import re
import time
from twitchobserver import Observer
from engine import roll
from config import CHANNEL, OAUTH, USERNAME


def proceed_mesage(observer, event):
    text = event.message

    if re.match(r'(?i)(\d*)(d|к)(4|6|8|100|10|12|20)\s*\+?\s*(\-?\d*)', text):
        raw_groups = re.match(r'(?i)(\d*)(d|к)(4|6|8|100|10|12|20)\s*\+?\s*(\-?\d*)', text).groups()
        raw_numbers = raw_groups[:1] + raw_groups[2:]
        roll(observer, raw_numbers)


def main():
    with Observer(USERNAME, OAUTH) as observer:
        observer.join_channel(CHANNEL)

        while True:
            try:
                for event in observer.get_events():
                    if event.type == 'TWITCHCHATMESSAGE':
                        proceed_mesage(observer, event)
                time.sleep(2)

            except KeyboardInterrupt:
                observer.leave_channel(CHANNEL)
                break
