import random
from config import CHANNEL


def roll(observer, raw_groups):
    """
    Roll the dice. Returns result of multiplier, dice type and modifier
    """
    multiple, dice, modifier = get_numbers_from_groups(raw_groups)
    results_list = []
    dice_result = 0
    message = ""

    if multiple and multiple is not 0:
        for i in range(multiple):
            dice_result = random.randint(1, dice)
            results_list.append(dice_result)
    else:
        dice_result = random.randint(1, dice)
        results_list.append(dice_result)

    result = sum(results_list)
            
    if modifier:
        result = result + modifier

    if not multiple or multiple == 1:
        message = "|{0}|".format(dice_result)
    else:
        for number in results_list:
            message += "|{0}| + ".format(number)
        message = message[:-2]
    if modifier:
        message += " + {0} ".format(modifier)
    if (multiple and multiple != 1) or modifier:
        message += "= {0}".format(result)
    send(observer, message)


def send(observer, message):
    """
    Send proeeded message
    """
    observer.send_message(message, CHANNEL)

def get_numbers_from_groups(raw_groups):
    """
    Check and prepare given numbers
    """
    if raw_groups[0] == "":
        multiple = None
    else:
        multiple = int(raw_groups[0])

    dice = int(raw_groups[1])

    if raw_groups[2] == "":
        modifier = None
    else:
        modifier = int(raw_groups[2])

    return multiple, dice, modifier
